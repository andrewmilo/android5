/*
 * This is a sample application that checks the type of network connection a device is using.
 * ConnectivityManager is a class that answers queries about the state of network connectivity. 
 * It also notifies applications when network connectivity changes. 
 * */
package com.example.connectiontype;

import android.app.AlertDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void buttonClicked(View view) {

		ConnectivityManager cmanager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		NetworkInfo networkInfoObj = cmanager.getActiveNetworkInfo();

		String networkType = null;

		if (networkInfoObj != null && networkInfoObj.isConnected()) {

			if (networkInfoObj.getType() == ConnectivityManager.TYPE_WIFI)
				networkType = "WI-FI will be used for Internet";
			else if (networkInfoObj.getType() == ConnectivityManager.TYPE_MOBILE)
				networkType = "Cell will be used for Internet";
			else
				networkType = "Not cell or WI-FI";

		} else
			networkType = "Not Connected";

		// AlertDialog with the connection type
		new AlertDialog.Builder(this)
				.setTitle("Network Type")
				.setMessage(networkType)
				.show();
	}
}