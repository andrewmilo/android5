/*
 * Android Intent is an object carrying an intent ie. message from 
 * one component to another component with-in the application or outside the application.
 * This example shows you a practical use of using Intent objects to launch an Email client
 * to send an Email to the given recipients.
 */
package com.turntotech.intentsample;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.InputStream;

public class MainActivity extends Activity {

	Intent photoIntent;
	final static int GET_IMAGE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button startBtn = (Button) findViewById(R.id.sendEmail);		
		startBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				photoIntent();
			}
		});
	}

	private void photoIntent() {

		try {

			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, "Select a Photo"), GET_IMAGE);
	
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(MainActivity.this,
					"There is no photo gallery installed..", Toast.LENGTH_SHORT)
					.show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GET_IMAGE && resultCode == Activity.RESULT_OK) {

			if (data == null) return;

			// Get URI
			Uri photoUri = data.getData();

			// Query for URI
			Cursor cursor = getContentResolver().query(photoUri, null, null, null, null);

			cursor.moveToFirst(); // get first

			// Get location
			String s = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
		}
	}

}
