/*
 * This is an example of sensor usage in Android.
 * The Android platform provides several sensors that let you monitor the motion of a device.
 * Motion sensors are useful for monitoring device movement, such as tilt, shake, rotation, or swing. 
 */

package org.turntotech.sensorsample;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

// SensorEventListener - Used for receiving notifications from the SensorManager when sensor values have changed. 
public class MainActivity extends Activity implements SensorEventListener {

	  private SensorManager mSensorManager;
	  Sensor accelerometer;
	  Sensor magnetometer;
	  private TextView textView;
	  float[] mGravity = null;
	  float[] mGeomagnetic = null;
	  float orientation[] = null;

	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);

	    textView = (TextView) findViewById(R.id.textView);

	    mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
	    
	    //getDefaultSensor() Use this method to get the default sensor for a given type. 
	    accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	  }

	  protected void onResume() {
	    super.onResume();
	    
	    // Registers a SensorEventListener for the given sensor. 
	    mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
	  }

	  protected void onPause() {
	    super.onPause();
	    
	    // Unregisters a listener for all sensors.
	    mSensorManager.unregisterListener(this);
	  }
	 
	  public void onAccuracyChanged(Sensor sensor, int accuracy) {  }

	  public void onSensorChanged(SensorEvent event) {
		  
	    
		 String details = "Sensor Details:";
		  
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
			mGravity = event.values;
		}

	    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
	      mGeomagnetic = event.values;
	    }
	    
	    if (mGravity != null && mGeomagnetic != null) {
	      
	      float R[] = new float[9];
	      float I[] = new float[9];
	      boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
	      if (success) {
	    	  orientation = new float[3];
	    	  SensorManager.getOrientation(R, orientation);
	      }
	    }

		  if(orientation!=null) {

			  // Get Azimuth
			  float az = orientation[0];

			  // Convert Azimuth to degrees
			  float az_deg = (float) Math.toDegrees(az);
			  if (az_deg < 0.0f)
				  az_deg += 360.0f;

			  // Floor the result for more steady readings
			  az_deg = (float)Math.floor(az_deg);

			  // Azimuth values
			  if (az_deg == 0 || az_deg == 360)
				  details += "N";
			  if (az_deg > 0 && az_deg < 90)
				  details += "NE";
			  if (az_deg == 90)
				  details += "E";
			  if (az_deg > 90 && az_deg < 180)
				  details += "SE";
			  if (az_deg == 180)
				  details += "S";
			  if (az_deg > 180 && az_deg < 270)
				  details += "SW";
			  if (az_deg == 270)
				  details += "W";
			  if (az_deg > 270 && az_deg < 360)
				  details += "NW";
		  }

		  textView.setText(details);

	  }
	}